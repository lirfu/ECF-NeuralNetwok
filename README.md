# ECF - Neural network

## What is it?

This is an extension of the existing ECF library by adding a neural network
feature.

## What's ECF?

ECF is a C++ framework intended for application of any type of evolutionary
computation.
Learn more at their [site](http://ecf.zemris.fer.hr/) and check out their
[documentation](http://ecf.zemris.fer.hr/documentation.html) or the
[CSS pumped documentation](http://ecf.zemris.fer.hr/html/index.html).

## Who's responsible for this?
Firstly our mentor: [**Domagoj Jakobović, Ph. D. C. S.**](http://www.zemris.fer.hr/~yeti/)

Then the development team:
* Borna Bejuk
* Danijel Pavlek
* Ivona Škorjanc
* Juraj Fulir
* Kristijan Jaklinović
* Lucija Ulaga
* Maša Burda
* Rudolf Lovrenčić
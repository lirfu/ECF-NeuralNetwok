#ifndef MUTGAUSS_H_
#define MUTGAUSS_H_
#include <ecf/ECF.h>


/**
* \ingroup genotypes flpoint
* \brief FloatingPoint (NN) genotype: Mutation adds to the randomly chosen weight a value with Gaussian distribution.
*/
class MutGauss : public MutationOp
{
protected:
public:
	bool mutate(GenotypeP gene);
	bool initialize(StateP);
	void registerParameters(StateP);
};
typedef boost::shared_ptr<MutGauss> MutGaussP;
#endif /* MUTGAUSS_H_ */

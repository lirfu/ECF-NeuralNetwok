#ifndef MUTSCALENEURON_H_
#define MUTSCALENEURON_H_
#include <ecf/ECF.h>


/**
* \ingroup genotypes flpoint
* \brief NeuralNetwork genotype: Mutation where all weights of a randomly chosen neuron are scaled by a factor from 0.5 to 1.5 so that the weights don't change as radically.
*/
class MutScaleNeuron : public MutationOp
{
protected:
public:
	bool mutate(GenotypeP gene);
	bool initialize(StateP);
	void registerParameters(StateP);
};
typedef boost::shared_ptr<MutScaleNeuron> MutScaleNeuronP;

#endif /* MUTSCALENEURON_H_ */


﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;

namespace SpaceFlight.Simulation
{
    public class AiShipController : IShipController
    {
        private double[] weights;
        private int[] structure;
        private int noNeurons;
        private int noLayers;
        private double[] neurons;

        public AiShipController(string s)
        {
            int index = s.IndexOf(";");
            string str = s.Substring(0, index);
            structure = Array.ConvertAll(str.Split(' '), int.Parse);
            s = s.Substring(index+1);

            //parsiranje stringa u double-ove
            try
            {
                weights = s.Split(' ').Select(i => double.Parse(i, CultureInfo.InvariantCulture)).ToArray();
            }
            catch (Exception)
            {

                weights = s.Split('\t').Select(i => double.Parse(i, CultureInfo.InvariantCulture)).ToArray();

            }
            
            noLayers = structure.Length;

            //broj neurona
            noNeurons = 0;
            for (int i = 0; i < structure.Length; i++)
            {
                noNeurons += structure[i];
            }
            neurons = new double[noNeurons];
        }
        //buduci da mrezu ne zanima znacenje parametara, ne treba ih stavljati u mapu i moze se odmah racunati s njima
        public void UpdateShip(TestCase testCase, Vector2D gravity)
        {
            double[] context = new double[8];

            Vector2D target = testCase.Waypoints[testCase.TargetWaypoint] - testCase.Ship.Position;
            context[0] = target.Length;
            context[1] = Vector2D.Angle(testCase.Ship.Direction, target);

            context[2] = testCase.Ship.Velocity.Length;
            context[3] = Vector2D.Angle(testCase.Ship.Direction, testCase.Ship.Velocity);
            context[4] = gravity.Length;
            context[5] = Vector2D.Angle(testCase.Ship.Direction, gravity);

            var nearest = testCase.Planets
              .DefaultIfEmpty(new Planet { Position = new Vector2D { X = -2000, Y = -2000 } })
              .Select(p => new { Distance = p.Position - testCase.Ship.Position, Raidus = p.Radius })
              .OrderBy(p => p.Distance.Length - p.Raidus)
              .First();

            context[6] = nearest.Distance.Length - nearest.Raidus;
            context[7] = Vector2D.Angle(testCase.Ship.Direction, nearest.Distance);

            testCase.Ship.Turn = NN_evaluate(context);
        }

        private double[] getWeights(int layer, int neuron)
        {
            double[] w = new double[structure[layer-1] +1];
            int position = 0;
            int l = 0;
            for (l = 1; l < layer; l++)
            {
                position += (1 + structure[l - 1]) * structure[l];
            }
            position += (1 + structure[l - 1]) * neuron;

            for (uint i = 0; i < 1 + structure[l - 1]; i++)
            {
                w[i] = (weights[position + i]);
            }
            return w;
        }

        //racuna izlaz mreze
        private double NN_evaluate(double[] context)
        {
            //prepisivanje vrijednosti ulaznih neurona
            for (int i = 0; i < context.Length; i++)
            {
                neurons[i] = context[i];
            }

            //petlja po slojevima (prvi preskacemo jer je to samo input)
            for (int i = 1; i < structure.Length; i++)
            {
                //petlja po neuronima u sloju
                for (int j = 0; j < structure[i]; j++)
                {
                    double[] w = getWeights(i, j);
                    int previousNeurons = 0;
                    for (int l = 0; l < i - 1; l++)
                    {
                        previousNeurons += structure[l];
                    }
                    int neurNo = previousNeurons + structure[i - 1] + j;
                    neurons[neurNo] = w[0]; //bias
                    for (int k = 1; k < w.Length; k++)
                    {
                        double inputValue = neurons[k + previousNeurons];
                        neurons[neurNo] += w[k] * inputValue;
                    }
                    if (i != structure.Length - 1)
                    {
                        neurons[neurNo] = 1 / (1 + Math.Exp(-neurons[neurNo]));
                    }
                }
            }
            return neurons[neurons.Length - 1];
        }
    }
}

#include <ECF/ECF.h>
#include <vcclr.h>
#include <msclr\marshal_cppstd.h>
#include "NeuralNetwork\NeuralNetwork.h"

using namespace SpaceFlight::Simulation;
using namespace msclr::interop;

class SpaceFlightEvalOp : public EvaluateOp
{
public:
	void registerParameters(StateP state)
	{
		state->getRegistry()->registerEntry("testcases.dir", static_cast<voidP>(new std::string), ECF::STRING);
	}

	bool initialize(StateP state)
	{

		if (!state->getRegistry()->isModified("testcases.dir"))
		{
			return false;
		}
		try
		{
			std::string *dir = static_cast<std::string *>(state->getRegistry()->getEntry("testcases.dir").get());
			evaluator = gcnew Evaluator(marshal_as<System::String ^>(*dir));
			return true;
		}
		catch (System::Exception ^e)
		{
			printf("Error: %s\n", marshal_as<std::string>(e->Message).c_str());
			return false;
		}
	}

	FitnessP evaluate(IndividualP individual)
	{
		NeuralNetwork* nn = (NeuralNetwork*)individual->getGenotype().get();
		std::string weights, structure, send;

		for (unsigned i = 0; i < nn->realValue.size(); ++i)
		{
			weights += dbl2str(nn->realValue[i]);
			weights += " ";
		}
		weights = weights.substr(0, weights.size() -1);

		for (unsigned i = 0; i < nn->getNoLayers(); ++i)
		{
			structure += uint2str(nn->getLayerStructure()[i]);
			structure += " ";
		}
		structure = structure.substr(0, structure.size() - 1);

		send = structure + ";" + weights;

		AiShipController ^controller = gcnew AiShipController(marshal_as<System::String ^>(send));
		FitnessP fitness = static_cast<FitnessP>(new FitnessMax);
		fitness->setValue(evaluator->Evaluate(controller) * 1000);
		return fitness;
	}

private:
	gcroot<Evaluator ^> evaluator;
};

int main(int argc, char **argv)
{
	NeuralNetworkP nn(new NeuralNetwork);

	StateP state = static_cast<StateP>(new State);
	state->addGenotype(nn);
	state->setEvalOp(static_cast<EvaluateOpP>(new SpaceFlightEvalOp));
	state->initialize(argc, argv);
	state->run();
	return 0;
}
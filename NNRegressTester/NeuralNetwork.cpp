#include <ecf/ECF.h>
#include <math.h>
#include <string.h>
#include "NeuralNetwork.h"

// Returns a copy of this genotype.
NeuralNetwork* NeuralNetwork::copy()
{
  NeuralNetwork *newObject = new NeuralNetwork(*this);
  return newObject;
}

void NeuralNetwork::registerParameters(StateP state)
{
  FloatingPoint::registerParameters(state);
  // XML will now support this entry as well.
  registerParameter(state, "structure",   (voidP) (new string("2 3 1")), ECF::STRING, "number of neurons per layer (default: 2 3 1)");
  registerParameter(state, "outFunction", (voidP) new uint(1),           ECF::UINT,   "activation function of the output layer (default: 1)");
  registerParameter(state, "configFile",  (voidP) (new string()),        ECF::STRING, "path to file containing the NN structure and weight values");
  registerParameter(state, "dataFile",    (voidP) (new string()),        ECF::STRING, "path to file containing training data (mandatory)");
  registerParameter(state, "errorWeights",(voidP) (new string()),        ECF::STRING, "path to file containing weights for every training data line.");
  registerParameter(state, "errorFunc",   (voidP) (new string("RMSE")),  ECF::STRING, "tag for error function used in getTotalError (default: RMSE)");
}

std::vector<MutationOpP> NeuralNetwork::getMutationOp()
{
  std::vector<MutationOpP> mut;
  mut = FloatingPoint::getMutationOp();

  // mut.push_back( (MutationOpP) (new NeuronMutOp));
  return mut;
}

// A special constructor for configuring the NN from a config file.
// State is not necceceraly valid, just need a dummy for error logging and stuff.
NeuralNetwork::NeuralNetwork(StateP state, string configFilePath)
{
  readConfigFile(state, configFilePath);
}

bool NeuralNetwork::initialize(StateP state)
{
  // GET parameters values
  voidP sptr = getParameterValue(state, "structure");
  string structure = *((string*) sptr.get());

  sptr = getParameterValue(state, "outFunction");
  outFunction_ = *((uint*) sptr.get());

  sptr = getParameterValue(state, "configFile");
  string config = *((string*) sptr.get());


  if(outFunction_ < 0)
  {
    ECF_LOG_ERROR(state, "Error: Output function index must be a positive number!");
    throw("");
  }

  if(config.empty())
  {
    // VALIDATE parameters definition
    if(structure.empty())
    {
      ECF_LOG_ERROR(state, "Error: structure for NN must be set (format: 2-3-1)!");
      throw("");
    }

    uint dimensionsNum = parseStructure(structure);

    if(npLayer_.size() < 2)
    {
      ECF_LOG_ERROR(state, "Error: NN structure must have at least 2 layers (input-output)!");
      throw("");
    }

    // Set the vector dimension and initialize the FloatingPoint parent
    Genotype::setParameterValue(state, std::string("dimension"), (voidP) (new uint(dimensionsNum)));
  }
  else
  {
    readConfigFile(state, config);
  }

  // Training data and parameters.
  if(isParameterDefined(state, "dataFile") && inputData_.size() == 0)
  {
    sptr = getParameterValue(state, "errorFunc");
    errorFunc_ = *((string*) sptr.get());

    sptr = getParameterValue(state, "dataFile");
    readDataFile(*((string*) sptr.get()));

    if(isParameterDefined(state, "errorWeights"))
    {
      sptr = getParameterValue(state, "errorWeights");
      readErrorWeights(*((string*) sptr.get()));
    }
    else
    {
      for(uint i=0; i<inputData_.size(); i++)
        errorWeights_.push_back(1);
    }
  }

  FloatingPoint::initialize(state);

  return true;
}

// Fills the npLayer_ vector with parsed values of given string.
// Returns the dimension of needed FloatingPoint to store all weights and biases.
uint NeuralNetwork::parseStructure(std::string structure)
{
  // Calculates the needed dimensions
  uint dimensionsNum = 0;
  // Read the structure (number of neurons per layer).
  string buffer = "";
  for(size_t i = 0; i < structure.length()+1; i++){
    if( ( i == structure.length() || isspace(structure[i]) ) && !buffer.empty() )
    {
      uint value = stoi(buffer);
      if(value <= 0)
      {
        throw std::invalid_argument("Error: number of neurons per layer must be > 0!");
      }
      npLayer_.push_back(value);

      // Reset buffer
      buffer = "";

      // Input layer has no weights
      if(npLayer_.size() > 1)
      {
        // Multiply the amount of neurons in last layer (+ 1 bias) with the amount of neurons in
        // this layer
        dimensionsNum += ( 1 + npLayer_[npLayer_.size()-2] ) * npLayer_[npLayer_.size()-1];
      }
    }
    else
      buffer += structure[i];
  }
  return dimensionsNum;
}

// Uses the given inputs to calculate the NN output.
// The inputs dimensions must be same as number of NN inputs.
// inputs -> Pointer to the vector containing values for input.
std::vector<double> NeuralNetwork::outputsFor(std::vector<double>* inputs)
{
  if(inputs->size() != getNoInputs()){
    throw std::invalid_argument("Error: number of given inputs differs from number of NN inputs! "+uint2str(inputs->size()));
  }

  // The outputs of a single layer.
  std::vector<double> lastOutputs = *inputs;
  uint pointer = 0;
  for(uint layer=1; layer<getNoLayers(); layer++)
  {
    // Outputs for neurons in this layer
    std::vector<double> thisOutput;

    for(uint neuron=0; neuron<npLayer_[layer]; neuron++)
    {
      double bias = realValue[pointer++];
      // The net value for this neuron, initially it's set to neurons bias value
      double net = bias;

      for(uint weight=0; weight<lastOutputs.size(); weight++)
      {
        net += realValue[pointer++] * lastOutputs[weight];
      }

      // Implement this neurons activation function
      net = neuronOutputFor(net, layer, bias);

      thisOutput.push_back(net);
    }

    // This layers outputs become the next layers inputs
    lastOutputs = thisOutput;
  }

  // The last layers outputs are the NN outputs
  return lastOutputs;
}

// Calculates the result of the activation function.
// net -> The sum of weights multiplied with appropriate last
//        layers neurons outputs + this neurons bias.
// layer -> Layer index of this neuron (For desciding which
//					activation function to use).
// bias -> This neurons bias value (for the 'net' correction).
double NeuralNetwork::neuronOutputFor(double net, uint layer, double bias)
{
  double output;
  if(layer == getNoLayers()-1) // The output layers scaling function is :
  {
    switch(outFunction_)
    {
      case 1:
        output = net; // identity function
      case 2:
        output = (net-bias) * bias; // w * sum
      case 3:
        output = tan(net); // tangent
      default:
        output = net; // identity
    }
  }
  else
  {
    output = 1. / (1 + exp(-net)); // SIGMODAL

    // Only for hidden layers the negative output is zeroed.
    if(output < 0)
      output = 0;
  }
// cout<<"\nL"<<layer<<"Output: "<<output;
  return output;
}

// Gets the weights for a parcitular neuron, including the bias.
// layer -> The layer in which the neuron is located. (2 to n because the
//          1st layer has no weights.)
// neuron -> The index of that neuron in selected layer. (1 to n)
// weights -> Pointer to a vector to be filled with values of weights
void NeuralNetwork::getWeights(uint layer, uint neuron, vector<double>* weights)
{
  if(layer < 2 || layer > getNoLayers())
  {
//    throw std::invalid_argument("Error: Cannot read weights, invalid layer index. Given: "+std::to_string(layer));
	  throw std::invalid_argument("Error: Cannot read weights, invalid layer index. Given: "+uint2str(layer));
  }
  if(neuron < 1 || neuron > npLayer_[layer-1])
  {
    throw std::invalid_argument("Error: Cannot read weights, invalid neuron index. Given: "+uint2str(neuron));
  }

  // Correct the indexes (inputs are indexed with 1..n, arrays are indexed with 0..n-1)
  layer--;
  neuron--;

  uint position = 0; // FP pointer

  uint l = 0;
  for(l=1; l<layer; l++)
    position += (1+npLayer_[l-1]) * npLayer_[l];
  position += (1+npLayer_[l-1]) * neuron;

  for(uint i=0; i<1+npLayer_[l-1]; i++)
    weights->push_back(realValue[position+i]);
}

// Sets the weights for a parcitular neuron, including the bias.
// layer -> The layer in which the neuron is located. (2 to n because the
//          1st layer has no weights.)
// neuron -> The index of that neuron in selected layer. (1 to n)
// weights -> Pointer to a vector filled with correct number of values for weights
void NeuralNetwork::setWeights(uint layer, uint neuron, vector<double>* weights)
{
  if(layer < 2 || layer > getNoLayers())
  {
	  throw std::invalid_argument("Error: Cannot read weights, invalid layer index. Given: "+uint2str(layer));
  }
  if(neuron < 1 || neuron > npLayer_[layer-1])
  {
    throw std::invalid_argument("Error: Cannot read weights, invalid neuron index. Given: "+uint2str(neuron));
  }
  if(weights->size() != 1+npLayer_[layer-2])
  {
    throw std::invalid_argument("Error: Insufficient number of weights. Given: "+uint2str(weights->size()));
  }

  // Correct the indexes (inputs are indexed with 1..n, arrays are indexed with 0..n-1)
  layer--;
  neuron--;

  uint position = 0; // FP pointer

  uint l = 0;
  for(l=1; l<layer; l++)
    position += (1+npLayer_[l-1]) * npLayer_[l];
  position += (1+npLayer_[l-1]) * neuron;

  for(uint i=0; i<1+npLayer_[l-1]; i++)
    realValue[position+i] = (*weights)[i];
}

// Reads the structure and weights bounds and values from a file.
bool NeuralNetwork::readConfigFile(StateP state, string dataFilepath)
{
  uint dimensions = 0;

  // Open input stream.
	ifstream file;
  file.open (dataFilepath);

  if (file.is_open())
  {
    // cout<<"\nReading from file: "<<dataFilepath;
    string line;
    // Read the structure
    if ( getline (file, line) && !line.empty() )
    {
      // cout<<"\nStructure: "<<line;
      dimensions = parseStructure(line);
      // cout<<"\nDimensions: "<<dimensions;
    }

    if ( getline (file, line) && !line.empty())
    {
      string buffer = "";
      uint i = 0;
      // Read the lower bound.
      while(!isspace(line[i]))
      {
        buffer += line[i];
        i++;
      }
      // The weight bounds
      Genotype::setParameterValue(state, std::string("lbound"), (voidP) (new double(stod(buffer))));
      // cout<<"\nLower bound: "<<buffer;

      buffer="";
      // Read the upper bound.
      for(uint j=i; j<line.length(); j++)
        buffer += line[j];
      Genotype::setParameterValue(state, std::string("ubound"), (voidP) (new double(stod(buffer))));
      // cout<<"\nUpper bound: "<<buffer;
    }

    // Set the vector dimension and initialize the FloatingPoint parent
    Genotype::setParameterValue(state, std::string("dimension"), (voidP) (new uint(dimensions)));

    // For each layer read and store weights
    std::vector<double> values;
    for(uint i=0; i<npLayer_.size(); i++)
    {
      if ( getline (file, line) && !line.empty() )
      {
        // Parse the values from string.
				string buffer = "";
				for(uint i = 0; i < line.length()+1; i++)
				{
					if( ( i==line.length() || isspace(line[i]) ) && !buffer.empty() )
					{
						values.push_back(stod(buffer));
						// Reset buffer
						buffer = "";
					}
					else
						buffer += line[i];
				}
      }
    }

    if(dimensions != values.size())
      throw std::invalid_argument("Error: Insufficient number of weights. Given: "+uint2str(values.size()));

    realValue = values;

    // cout<<"\n\n";
    file.close();
  }
	else
 	{
			return false;
 	}
  return true;
}

// Saves the structure and weight bounds and values to a file.
bool NeuralNetwork::generateConfigFile(std::string filePath)
{
  ofstream file (filePath);
  if (file.is_open())
  {
    // file << "This is a line.\n";
    for(uint i=0; i<npLayer_.size(); i++)
      file << npLayer_[i] << " ";
    file << "\n" << getLBound() << " " << getUBound() << "\n";
    for(uint i=0; i<realValue.size(); i++)
      file << realValue[i] << " ";

    file.close();
  }
  else
  {
    cout << "Unable to open file";
    file.close();
    return false;
  }

  return true;
}

// Reads the data (inputs and outputs) for calculations.
bool NeuralNetwork::readDataFile(std::string filePath)
{
  // Open input stream.
	ifstream file;
  file.open (filePath);

	if (file.is_open())
 	{
    uint nInputs = getNoInputs();

		string line;
		while ( getline (file, line) )
	 	{
			if(!line.empty())
			{
				std::vector<double> inp;
        std::vector<double> out;

				// Parse the values from string.
				string buffer = "";
				for(uint i = 0; i < line.length()+1; i++)
				{
          // Delimiter
					if(i==line.length() || isspace(line[i]))
					{
						double value = stod(buffer);

            if(inp.size() < nInputs)
            {
              inp.push_back(value);
            } else {
              out.push_back(value);
            }

            // Reset buffer
						buffer = "";
					}
					else
						buffer += line[i];
				}
        inputData_.push_back(inp);
        outputData_.push_back(out);
			}
	 	}
	 	file.close();

    if(inputData_.size() != outputData_.size())
    {
      throw std::invalid_argument("NeuralNetwork Error: number of inputs and outputs isn't equal");
    }
 	}
	else
 	{
      throw std::invalid_argument("NeuralNetwork Error: unable to open data file: "+filePath);
			return false;
 	}
	return true;
}

// Reads the weights for each line of inputs.
void NeuralNetwork::readErrorWeights(std::string filePath)
{
    // Open input stream.
  ifstream file;
  file.open (filePath);

  if (file.is_open())
  {
    double inputs_sum = 0;

    string line;
    // Parse the weights from file (one weight per line).
    while ( getline (file, line) )
    {
      if(!line.empty())
      {
        double value = stod(line);
        errorWeights_.push_back(value);
        inputs_sum += value;
      }
    }
    file.close();

    if(errorWeights_.size() < inputData_.size())
    {
      throw std::invalid_argument("NeuralNetwork Error: errorWeights file hasn't got enough weights");
    }

    // Turn the weights into percentages.
    for(uint i = 0; i < errorWeights_.size(); i++)
      errorWeights_[i] = errorWeights_[i] / inputs_sum;
  }
  else
  {
      throw std::invalid_argument("NeuralNetwork Error: unable to open errorWeights file: "+filePath);
  }
}

// Calculates the fitness of this NN
double NeuralNetwork::getTotalError()
{
  double totalError = 0;

  for(uint dataIt=0; dataIt < inputData_.size(); dataIt++)
  {
    std::vector<double> result = outputData_[dataIt];
    std::vector<double> output = outputsFor(&(inputData_[dataIt]));

    double error=0;
    if(errorFunc_.compare("MSE") == 0)
    {
        for(uint i=0; i<getNoOutputs(); i++)
        {
          error += (output[i] - result[i])*(output[i] - result[i]);
        }
        error /= getNoOutputs();
    } else if (errorFunc_.compare("RMSE") == 0) {
        for(uint i=0; i<getNoOutputs(); i++)
        {
          error += (output[i] - result[i])*(output[i] - result[i]);
        }
        error /= getNoOutputs();
        error = sqrt(error);
    } else if (errorFunc_.compare("MAE") == 0) {
        for(uint i=0; i<getNoOutputs(); i++)
        {
          error += abs(output[i] - result[i]);
        }
        error /= getNoOutputs();
    } else {
        for(uint i=0; i<getNoOutputs(); i++)
        {
          error += (output[i] - result[i])*(output[i] - result[i]);
        }
        error /= getNoOutputs();
        error = sqrt(error);
    }

    // Weight the importance of this error.
    totalError += error * errorWeights_[dataIt];
  }

  return totalError;
}

#include <ecf/ECF.h>
#include "NeuralNetwork.h"


int main(int argc, char **argv)
{
	StateP state (new State);
	NeuralNetwork f = NeuralNetwork(state, argv[1]);

	// double arr[] = {1,0,0,1};
	// vector<double> vec (arr, arr + sizeof(arr) / sizeof(arr[0]) );
	// f.setWeights(4, 1, &vec);

	cout << "\n>>>Time to use this program!<<<\n";
	while(true)
	{
		std::vector<double> inputs;
		double value;
		for(uint i=0; i<f.getInputsNumber(); i++)
		{
			cout << "\nEnter "<<i<<". argument(input): ";
			cin >> value;
			inputs.push_back(value);
		}

		std::vector<double> result = f.outputsFor(&inputs);
		cout << "\nOutput: ";
		for(uint i=0; i<f.getOutputsNumber(); i++)
			cout << "[" << result[i] << "] ";
	}
	cout << "\nEnded.";

	return 0;
}

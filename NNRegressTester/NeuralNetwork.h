#ifndef NEURALNETWORK_H_
#define NEURALNETWORK_H_

//
// Modified FP genotype
//
class NeuralNetwork : public FloatingPoint::FloatingPoint
{
public:
	// Constructor
	NeuralNetwork()
	{ name_ = "NeuralNetwork"; }

	// A special constructor for configuring the NN from a config file.
	// State is not necceceraly valid, just need a dummy for error logging and stuff.
	NeuralNetwork(StateP state, string configFilePath);

	// Methods
	NeuralNetwork* copy();
	void registerParameters(StateP state);
	std::vector<MutationOpP> getMutationOp();
	bool initialize(StateP state);

	// Number of NN layers.
	uint getNoLayers()
	{ return npLayer_.size(); }

	// Number of NN inputs.
	uint getNoInputs()
	{	return npLayer_[0]; }

	// Number of NN outputs.
	uint getNoOutputs()
	{ return npLayer_[getNoLayers()-1];	}

	// Gets the weights for a parcitular neuron, including the bias.
	// layer -> The layer in which the neuron is located. (2 to n because the
	//          1st layer has no weights.)
	// neuron -> The index of that neuron in selected layer. (1 to n)
	// weights -> Pointer to a vector to be filled with values of weights
	void getWeights(uint layer, uint neuron, vector<double>* weights);

	// Sets the weights for a parcitular neuron, including the bias.
	// layer -> The layer in which the neuron is located. (2 to n because the
	//          1st layer has no weights.)
	// neuron -> The index of that neuron in selected layer. (1 to n)
	// weights -> Pointer to a vector filled with correct number of values for weights
	void setWeights(uint layer, uint neuron, vector<double>* weights);

	// Uses the given inputs to calculate the NN output.
	// The inputs dimensions must be same as number of NN inputs.
	// inputs -> Pointer to the vector containing values for input.
	std::vector<double> outputsFor(std::vector<double>* inputs);

	// Reads the structure and weights bounds and values from a file.
	bool readConfigFile(StateP state, std::string filePath);

	// Saves the structure and weight bounds and values to a file.
	bool generateConfigFile(std::string filePath);

	// Reads the data (inputs and outputs) for calculations.
	bool readDataFile(std::string filePath);

	// Calculates the fitness of this NN
	double getTotalError();

	private:
		// Number of neurons per layer.
		std::vector<uint> npLayer_;
		// Index of the function used for output layer.
		uint outFunction_;

		// Tag for desciding which error function to use.
		std::string errorFunc_;
		// Weights for each input data line.
		std::vector<double> errorWeights_;

		// Data read from the data file (if given).
		std::vector<std::vector<double>> inputData_;
		std::vector<std::vector<double>> outputData_;

		// Calculates the result of the activation function.
		// net -> The sum of weights multiplied with appropriate last
		//        layers neurons outputs + this neurons bias.
		// layer -> Layer index of this neuron (For desciding which
		//					activation function to use).
		// bias -> This neurons bias value (for the 'net' correction).
		double neuronOutputFor(double net, uint layer, double bias);

		// Fills the npLayer_ vector with parsed values of given string.
		// Returns the dimension of needed FloatingPoint to store all weights and biases.
		uint parseStructure(std::string structure);

		// Reads the weights for each line of inputs.
		void readErrorWeights(std::string filePath);
};
typedef boost::shared_ptr<NeuralNetwork> NeuralNetworkP;

#endif
